CREATE TABLE document(
 id          BIGSERIAL PRIMARY KEY,
 employee_id BIGINT NOT NULL  REFERENCES employee(id),

 type            VARCHAR (32) NOT NULL,
 number          VARCHAR (64) NOT NULL,
 issue_date      Date,
 expiry_date     Date,

 created_date TIMESTAMP NOT NULL DEFAULT NOW(),
 updated_date TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE INDEX idx_fk_document_employee ON document(employee_id, id);
CREATE INDEX idx_document_number ON document(number, id);
