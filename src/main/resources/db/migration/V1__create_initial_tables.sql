CREATE TABLE company(
 id   BIGSERIAL PRIMARY KEY,
 name VARCHAR (256) NOT NULL,

 created_date TIMESTAMP NOT NULL DEFAULT NOW(),
 updated_date TIMESTAMP NOT NULL DEFAULT NOW()
);


CREATE TABLE country(
 country_code VARCHAR (4) PRIMARY KEY,
 name         VARCHAR (64) NOT NULL,
 nationality  VARCHAR (64) NOT NULL
);


CREATE TABLE employee(
 id BIGSERIAL PRIMARY KEY,

 title          VARCHAR (8)  NOT NULL,
 name           VARCHAR (64) NOT NULL,
 gender         VARCHAR (16) NOT NULL,
 date_of_birth  DATE NOT NULL,
 marital_status VARCHAR (32),
 personal_phone VARCHAR (32) NOT NULL,

 nationality               VARCHAR (64) NOT NULL,
 double_nationality_status VARCHAR (16) NOT NULL,
 second_nationality        VARCHAR (64),

 educational_level VARCHAR (32) NOT NULL,
 educational_area  VARCHAR (64) NOT NULL,

 tax_number    VARCHAR (64),
 social_number VARCHAR (64),
 tax_framework VARCHAR (64),
 iban          VARCHAR (64),
 swift_code    VARCHAR (64),

 status VARCHAR (32) NOT NULL,

 employee_number     VARCHAR (64),
 company_phone       VARCHAR (32),
 job_title           VARCHAR (64),
 internal_number     VARCHAR (64),
 organizational_unit VARCHAR (32),
 career_level        VARCHAR (32),
 direct_report_id    BIGINT REFERENCES employee(id),
 internal_email      VARCHAR (64),
 company_id          BIGINT REFERENCES company(id),

 active       BOOLEAN   NOT NULL,
 created_date TIMESTAMP NOT NULL DEFAULT NOW(),
 updated_date TIMESTAMP NOT NULL DEFAULT NOW()
);


CREATE TABLE dependent(
 id          BIGSERIAL PRIMARY KEY,
 employee_id BIGINT NOT NULL  REFERENCES employee(id),

 relationship    VARCHAR (32) NOT NULL,
 name            VARCHAR (64),
 living_portugal BOOLEAN NOT NULL,
 tax_number      VARCHAR (64),

 created_date TIMESTAMP NOT NULL DEFAULT NOW(),
 updated_date TIMESTAMP NOT NULL DEFAULT NOW()
);


CREATE TABLE address(
 id          BIGSERIAL PRIMARY KEY,
 employee_id BIGINT NOT NULL  REFERENCES employee(id),
 
 line1        VARCHAR (256) NOT NULL,
 line2        VARCHAR (256),
 district     VARCHAR (64),
 city         VARCHAR (64),
 country_code VARCHAR (4) REFERENCES country(country_code),
 postal_code  VARCHAR (16),
 
 created_date TIMESTAMP NOT NULL DEFAULT NOW(),
 updated_date TIMESTAMP NOT NULL DEFAULT NOW()
);


CREATE TABLE project(
 id          BIGSERIAL PRIMARY KEY,
 employee_id BIGINT NOT NULL  REFERENCES employee(id),
 
 location    VARCHAR (64) NOT NULL,
 start_date  DATE NOT NULL,
 end_date    DATE NOT NULL,

 created_date TIMESTAMP NOT NULL DEFAULT NOW(),
 updated_date TIMESTAMP NOT NULL DEFAULT NOW()
);


CREATE TABLE contract(
 id          BIGSERIAL PRIMARY KEY,
 employee_id BIGINT NOT NULL  REFERENCES employee(id),
 
 type VARCHAR (64) NOT NULL,

 expected_admission_date DATE NOT NULL,
 original_admission_date DATE,
 probation_period        DATE,
 end_date                DATE,

 created_date TIMESTAMP NOT NULL DEFAULT NOW(),
 updated_date TIMESTAMP NOT NULL DEFAULT NOW()
);


CREATE TABLE compensation(
 id          BIGSERIAL PRIMARY KEY,
 employee_id BIGINT NOT NULL  REFERENCES employee(id),
 
 type VARCHAR (64) NOT NULL,

 start_date  DATE NOT NULL,
 end_date    DATE NOT NULL,

 gross_amount DECIMAL(12,2),
 net_amount   DECIMAL(12,2),

 created_date TIMESTAMP NOT NULL DEFAULT NOW(),
 updated_date TIMESTAMP NOT NULL DEFAULT NOW()
);


CREATE TABLE insurance(
 id          BIGSERIAL PRIMARY KEY,
 employee_id BIGINT NOT NULL  REFERENCES employee(id),
 
 provider VARCHAR (64) NOT NULL,

 policy_number VARCHAR (64) NOT NULL,
 start_date    DATE NOT NULL,
 end_date    DATE NOT NULL,

 created_date TIMESTAMP NOT NULL DEFAULT NOW(),
 updated_date TIMESTAMP NOT NULL DEFAULT NOW()
);


INSERT INTO
  country (country_code, name, nationality)
VALUES
  ('ARG', 'Argentina', 'Argentine'),
  ('AUS', 'Australia', 'Australian'),
  ('AUT', 'Austria', 'Austrian'),
  ('BEL', 'Belgium', 'Belgian'),
  ('BOL', 'Bolivia', 'Bolivian'),
  ('BRA', 'Brazil', 'Brazilian'),
  ('BGR', 'Bulgaria', 'Bulgarian'),
  ('CPV', 'Cabo Verde', 'Cabo Verdean'),
  ('CAN', 'Canada', 'Canadian'),
  ('CHL', 'Chile', 'Chilean'),
  ('CHN', 'China', 'Chinese'),
  ('COL', 'Colombia', 'Colombian'),
  ('HRV', 'Croatia', 'Croatian'),
  ('CUB', 'Cuba', 'Cuban'),
  ('CZE', 'Czech Republic', 'Czech'),
  ('DNK', 'Denmark', 'Danish'),
  ('DOM', 'Dominican Republic', 'Dominican'),
  ('ECU', 'Ecuador', 'Ecuadorian'),
  ('EGY', 'Egypt', 'Egyptian'),
  ('FIN', 'Finland', 'Finnish'),
  ('FRA', 'France', 'French'),
  ('DEU', 'Germany', 'German'),
  ('GIB', 'Gibraltar', 'Gibraltar'),
  ('GRC', 'Greece', 'Greek, Hellenic'),
  ('HUN', 'Hungary', 'Hungarian, Magyar'),
  ('ISL', 'Iceland', 'Icelandic'),
  ('IRL', 'Ireland', 'Irish'),
  ('ITA', 'Italy', 'Italian'),
  ('JPN', 'Japan', 'Japanese'),
  ('LUX', 'Luxembourg', 'Luxembourg, Luxembourgish'),
  ('MEX', 'Mexico', 'Mexican'),
  ('NLD', 'Netherlands', 'Dutch, Netherlandic'),
  ('NOR', 'Norway', 'Norwegian'),
  ('PRY', 'Paraguay', 'Paraguayan'),
  ('PER', 'Peru', 'Peruvian'),
  ('POL', 'Poland', 'Polish'),
  ('PRT', 'Portugal', 'Portuguese'),
  ('SRB', 'Serbia', 'Serbian'),
  ('SGP', 'Singapore', 'Singaporean'),
  ('ESP', 'Spain', 'Spanish'),
  ('SWZ', 'Swaziland', 'Swazi'),
  ('SWE', 'Sweden', 'Swedish'),
  ('CHE', 'Switzerland', 'Swiss'),
  ('TUR', 'Turkey', 'Turkish'),
  ('UKR', 'Ukraine', 'Ukrainian'),
  ('GBR', 'United Kingdom', 'British, UK'),
  ('USA', 'United States', 'American'),
  ('URY', 'Uruguay', 'Uruguayan'),
  ('VEN', 'Venezuela', 'Venezuelan');
