CREATE INDEX idx_company_name ON company(name);

CREATE INDEX idx_fk_employee_direct_report_id ON employee(direct_report_id, id);
CREATE INDEX idx_fk_employee_internal_email ON employee(internal_email, id);
CREATE INDEX idx_employee_active ON employee(active, id);
CREATE INDEX idx_employee_status ON employee(status, id);

CREATE INDEX idx_fk_employee_id ON dependent(employee_id, id);
CREATE INDEX idx_dependent_living_portugal ON dependent(living_portugal, id);
CREATE INDEX idx_dependent_name ON dependent(name, id);

CREATE INDEX idx_fk_address_employee_id ON address(employee_id, id);
CREATE INDEX idx_address_country_code ON address(country_code, id);

CREATE INDEX idx_fk_project_employee_id ON project(employee_id, id);

CREATE INDEX idx_fk_contract_employee_id ON contract(employee_id, id);
CREATE INDEX idx_contract_type ON contract(type, id);

CREATE INDEX idx_fk_compensation_employee_id ON compensation(employee_id, id);
CREATE INDEX idx_compensation_type ON compensation(type, id);

CREATE INDEX idx_fk_insurance_employee_id ON insurance(employee_id, id);

CREATE INDEX idx_country_name_country_code ON country(name, country_code);
